resource "aws_security_group" "security_group" {
  name = "${var.prefix}_security_group"
  vpc_id = aws_vpc.vpc.id
}

resource "aws_security_group_rule" "rancher_ssh_ingress" {
  protocol = "tcp"
  from_port = 22
  to_port = 22
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group.id
  type = "ingress"
}

resource "aws_security_group_rule" "rancher_https_ingress" {
  protocol = "tcp"
  from_port = 443
  to_port = 443
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group.id
  type = "ingress"
}

resource "aws_security_group_rule" "rancher_egress_rule" {
  protocol = "-1"
  from_port = 0
  to_port = 0
  cidr_blocks = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group.id
  type = "egress"
}

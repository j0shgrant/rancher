terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/19898225/terraform/state/rancher"
    lock_address = "https://gitlab.com/api/v4/projects/19898225/terraform/state/rancher/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/19898225/terraform/state/rancher/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
}

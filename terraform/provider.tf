provider "aws" {
  version = "~> 2.0"
  region = var.aws_region
}

provider "tls" {
  version = "2.1.1"
}

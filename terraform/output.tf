output "rancher_public_ip" {
  value = aws_eip.elastic_ip.public_ip
}

output "rancher_instance_id" {
  value = aws_instance.rancher.id
}

output "rancher_private_key" {
  value = tls_private_key.global_key.private_key_pem
  sensitive = true
}

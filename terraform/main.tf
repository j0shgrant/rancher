resource "aws_key_pair" "deployment_key_pair" {
  key_name = "${var.prefix}_key_pair"
  public_key = tls_private_key.global_key.public_key_openssh
}

resource "aws_instance" "rancher" {
  ami = var.aws_ami
  instance_type = "t2.medium"
  availability_zone = var.aws_az
  key_name = aws_key_pair.deployment_key_pair.key_name
  vpc_security_group_ids = [aws_security_group.security_group.id]
  subnet_id = aws_subnet.subnet.id
}

variable "aws_region" {
  type        = string
  description = "AWS region used for all resources"
  default     = "eu-west-1"
}

variable "aws_az" {
  type = string
  description = "AWS AZ used for all resources"
  default = "eu-west-1b"
}

variable "aws_ami" {
  type = string
  description = "AMI for Ubuntu"
  default = "ami-0980f143956f4c4a0"
}

variable "prefix" {
  type = string
  description = "Generic prefix for all resources"
  default = "rancher"
}

#!/usr/bin/env sh
terraform init \
  -backend-config="username=${CI_REGISTRY_USER}" \
  -backend-config="password=${CI_REGISTRY_PASSWORD}"
